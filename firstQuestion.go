package main

import "fmt"
import "strconv"
import "sort"

func main() {
	
	input := []string{"aaaasd", "a", "aab", "aaabcd", "ef", "cssssssd", "fdz", "kf", "zc", "lklklklklklklklkl", "l"}

	// There are various of different input tests . Please try them .
	
	//input := []string{"lklklklklklklklkl","kf","aab","aartwygı","aahgk","cssssssd", "aaabcd","ef","l","aaaasd","fdz","a", "zc"}
	//input := []string{"a","aaab","aaaaageg","aa","aqwer","qwea","qaq",}
	//input := []string{"abv","abv","aabc"}
	//input := []string{"aglhalg","er","g"}
	//input := []string{"8aabn","4graaaaj","2tr"}
	//input := []string{"8/)aa%8","=$fa``aa","\175861761<"}

	MaxNumberA := 0;  							//// Integer for keeping max number of a's 

	for i := 0; i < len(input); i++ {       	//// This part counts the number of a's and writes 
		NumberOfA := 0;                     	//// total number at the beginning of string . 
		
		for _, r := range input[i] {        	//// Iterating each char of strings
			
			if r == 97{                     	//// Check is it 'a'
				NumberOfA++                 	//// If it is 'a' increase NumberOfA integer
			}
		}
		if NumberOfA>MaxNumberA {				//// Updating max number of a'S
			MaxNumberA = NumberOfA
		}
		newStr := strconv.Itoa(NumberOfA)       //// Converting NumberOfA integer to string
		input[i]=  newStr+input[i]              //// Adding NumberOfA at the beginnig of string 
	}
	
	
	sort.Slice(input, func(i, j int) bool {     //// Sorting strings according to number of a's
		return input[i] > input[j]
	})

	trash := []string{}                         //// trash array list will be used for adding sorted strings
	finalString := []string{}                   //// finalString array list will be used for collect all sorted strings

	for i := MaxNumberA; i >= 0 ; i-- {		    /// This loop repeat for each different a numbers (MaxNumberA: 4;3;2;1;0)
		trash = nil                             /// Clear the trash array list
		for k := 0; k < len(input); k++ {       /// This loop iterate all strings
			aNumber :=0		                    /// Number of a'S the string has
			
			for _, r := range input[k] { 		/// Iterate each char in strings
				if r == 97 {					/// Counting the number of a's
					aNumber++
				}
			}
	
			if aNumber==i {                     /// if number of a'S string has equal to i,
				trash = append(trash,input[k])  /// code add string to trash array list
			}	            
		}
		sort.Sort(ByLen(trash))                     /// Strings in the trash list are sorted by length
		finalString = append(finalString,trash...)  /// Sorted strings added to finalString
	}
	
	for i := 0; i < len(finalString); i++ {
		finalString[i]=trimLeftChar(finalString[i])            //// Removing numbers from strings
	}
	
	fmt.Println("Final String:")
	fmt.Println(finalString)                                   //// Results :)

}

func trimLeftChar(s string) string {						 
    for i := range s {
        if i > 0 {
            return s[i:]
        }
    }
    return s[:0]
}

type ByLen []string
 
func (a ByLen) Len() int {
   return len(a)
}
 
func (a ByLen) Less(i, j int) bool {
   return len(a[i]) > len(a[j])
}
 
func (a ByLen) Swap(i, j int) {
   a[i], a[j] = a[j], a[i]
}
